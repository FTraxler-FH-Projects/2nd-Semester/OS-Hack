# Solution script for OS-Fernlehre 3
# by Florian Traxler - 2018

import threading as t
import subprocess as sproc
import glob
import os

# fields
s_fuckinglinter = t.Semaphore(2)

# run some shell command
def run(cmd):
    proc = sproc.Popen([cmd], shell=True, stdout=sproc.PIPE)
    return proc.communicate()

# run th calculation script
def run_calc(value):
    (out) = run('./calc.sh ' + str(value))
    return int(out)

# read data from a file
def readfile(name):
    content = []
    with open(name, "r") as file:
        for line in file:
            content.append(int(line.strip()))
    return content

# write data into a file
def writefile(name, content):
    with open(name, "w") as file:
        file.write(content)

# slowsort algorithm
def slowsort(array, i, ii):
    # print(arr, i, j)
    if i >= ii:
        return

    m_fuckinglinter = (int)((i + ii) / 2)

    slowsort(array, i, m_fuckinglinter)
    slowsort(array, m_fuckinglinter + 1, ii)

    if array[ii] < array[m_fuckinglinter]:
        t_fuckinglinter = array[ii]
        array[ii] = array[m_fuckinglinter]
        array[m_fuckinglinter] = t_fuckinglinter

    slowsort(array, i, ii - 1)

# slowsort helper function
def do_work(name):
    s_fuckinglinter.acquire()

    array = []
    for i in readfile(name):
        array.append(run_calc(i))
    slowsort(array, 0, len(array) - 1)

    temp_name = os.path.basename(name).replace(".csv", "").split("-")
    target_file = "%d-%d.csv" % (int(temp_name[0]) * 2, int(temp_name[1]) * 2)

    content = ''
    for i in array:
        content += str(i) + '\n'
    writefile(target_file, content)

    run('chmod 600 ' + target_file)

    s_fuckinglinter.release()

### ---------------------------------------------------------- ###
#                   M A I N   P R O G R A M
### ---------------------------------------------------------- ###
# setup workspace
for filename in sorted(glob.glob("*.csv")):
    os.remove(filename)
run('chmod +x ./gencsv.sh ./calc.sh')
run('./gencsv.sh')

# run program multithreaded
threads = []
for filename in sorted(glob.glob("*.csv")):
    thread = t.Thread(target=do_work, args=(filename,))
    thread.start()
    threads.append(thread)

for thread in threads:
    thread.join()
### ---------------------------------------------------------- ###